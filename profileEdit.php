<?php
include './navigation.php';
include_once './vendor/autoload.php';

use App\BITM\SEIP108594\users\Users;

//session_start();
if (isset($_SESSION['Message'])) {
    echo $_SESSION['Message'];
    unset($_SESSION['Message']);
}
//if(isset($_SESSION['id'])){
//    $username = $_SESSION['username'];
//    echo 'Welcome '.$username.' to our website';
//}
$id = $_POST['id'];
//echo $_POST['id'];
//die();
$user = new Users();
$oneUser = $user->showProfile($id);
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Mini Project| User Profile</title>

    </head>
    <body>
        <div class="container">
            <section style="padding-bottom: 50px; padding-top: 50px;">
                <div class="row">
                    <div class="col-md-4">
                        <img src="<?php echo "img/" . $oneUser['profile_pic'] ?>" width="250" height="150" class="img-rounded img-responsive" />
                        <br/>
                        <h4><b>Profile Picture</b></h4>
                    </div>
                    <div class="col-md-8">
                        <div class="alert alert-info">
                            <h4>Update User Information</h4>
                            <!--<h4>Bootstrap user profile template </h4>-->
                            
                        </div>
                        
                        <br>
                        <div class="form-group col-md-8">
                            <table class="table table-striped table-hover">
                                <form action="view/BITM/SEIP108594/users/profileUpdate.php" method="post">
                                    <input type="hidden" name="id" value="<?php echo $id ?>">
                                    <tr><td><label for="firstName">First Name</label> </td> <td>
                                            <input type="text" name="firstName" id="firstName" size="40" placeholder="No Data Available" value="<?php
                                            if (isset($oneUser['first_name']) && !empty($oneUser['first_name'])) {
                                                echo $oneUser['first_name'];
                                            }
                                            ?>"></td></tr>
                                    <tr><td><label for="lastName">Last Name</label> </td> <td>
                                            <input type="text" name="lastName" id="lastName" size="40" placeholder="No Data Available" value="<?php
                                            if (isset($oneUser['last_name']) && !empty($oneUser['last_name'])) {
                                                echo $oneUser['last_name'];
                                            }
                                            ?>"></td></tr>
                                    <tr><td><label for="personalPhone">Personal Phone</label> </td> <td>
                                            <input type="text" name="personalPhone" id="personalPhone" size="40" placeholder="No Data Available" value="<?php
                                            if (isset($oneUser['personal_phone']) && !empty($oneUser['personal_phone'])) {
                                                echo $oneUser['personal_phone'];
                                            }
                                            ?>"></td></tr>
                                    <tr><td><label for="homePhone">Home Phone</label> </td> <td>
                                            <input type="text" name="homePhone" id="homePhone" size="40" placeholder="No Data Available" value="<?php
                                            if (isset($oneUser['home_phone']) && !empty($oneUser['home_phone'])) {
                                                echo $oneUser['home_phone'];
                                            }
                                            ?>"></td></tr>
                                    <tr><td><label for="officePhone">Office Phone</label> </td> <td>
                                            <input type="text" name="officePhone" id="officePhone" size="40" placeholder="No Data Available" value="<?php
                                            if (isset($oneUser['office_phone']) && !empty($oneUser['office_phone'])) {
                                                echo $oneUser['office_phone'];
                                            }
                                            ?>"></td></tr>
                                    <tr><td><label for="currentAddress">Current Address</label> </td> <td>
                                            <textarea name="currentAddress" id="currentAddress" rows="5" cols="42"><?php
                                                if (isset($oneUser['current_address']) && !empty($oneUser['current_address'])) {
                                                    echo $oneUser['current_address'];
                                                }
                                                ?></textarea></td></tr>
                                    <tr><td><label for="permanentAddress">Permanent Address</label> </td> <td>
                                            <textarea name="permanentAddress" id="permanentAddress" rows="5" cols="42"><?php
                                                if (isset($oneUser['permanent_address']) && !empty($oneUser['permanent_address'])) {
                                                    echo $oneUser['permanent_address'];
                                                }
                                                ?></textarea></td></tr>


                                    <tr><td></td><td>  <input class="btn btn-success pull-right"type="submit" value="Update Profile"></td></tr>
                                </form>
                            </table>
                        </div>
                    </div>
                </div>
                <!-- ROW END -->
 

            </section>
            <!-- SECTION END -->
        </div>
        <!-- CONATINER END -->

    </body>
</html>
