<?php
include_once '../../../../vendor/autoload.php';

use App\BITM\SEIP108594\users\Users;

session_start();
if (isset($_SESSION['Message'])) {
    echo $_SESSION['Message'];
    unset($_SESSION['Message']);
}
//echo $_POST['id'];
//print_r($_POST);
//die();
if (isset($_FILES['picture'])) {
    $errors = array();
    $pictureName = time().$_FILES['picture']['name'];
    $pictureType = $_FILES['picture']['type'];
    $pictureTempName = $_FILES['picture']['tmp_name'];
    $pictureSize = $_FILES['picture']['size'];
    $explodedName = explode('.', $pictureName);
    $fileExtension = strtolower(end($explodedName));
    
    $format = array('jpeg','jpg','png');
    if (in_array($fileExtension, $format) === FALSE){
        $errors = "Wrong Format";
    }
    if (empty($errors)===TRUE) {
        move_uploaded_file($pictureTempName, "../../../../img/".$pictureName);
        $_POST['picture'] = $pictureName;
    }
}
$user = new Users();
$user->prepare($_POST);
$user->updateProfilePicture();
