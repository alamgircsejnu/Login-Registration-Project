<?php
include './navigation.php';
include_once './vendor/autoload.php';

use App\BITM\SEIP108594\users\Users;

//session_start();
if (isset($_SESSION['Message'])) {
    echo $_SESSION['Message'];
    unset($_SESSION['Message']);
}
//if(isset($_SESSION['id'])){
//    $username = $_SESSION['username'];
//    echo 'Welcome '.$username.' to our website';
//}
if (isset($_GET['id']) && !empty($_GET['id'])) {
    $id = $_GET['id'];
} else {
    $id = $_SESSION['id'];
}
//echo $_GET['id'];
//echo $_SESSION['id'];
//die();
$user = new Users();
$oneUser = $user->showProfile($id);
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Mini Project| User Profile</title>

    </head>
    <body>
        <div class="container">
            <section style="padding-bottom: 50px; padding-top: 50px;">
                <div class="row">
                    <div class="col-md-4">
                        <img src="<?php echo "img/" . $oneUser['profile_pic'] ?>" width="250" height="150" class="img-rounded img-responsive" />
                        <br/><br/>
                        <form action="profilePictureEdit.php" method="post">
                                <input type="hidden" name="id" value="<?php echo $id ?>">
                                <?php if ($id == $_SESSION['id']) { ?>
                                    <input class="btn btn-success" type="submit" value="Edit Profile Picture">
                                <?php } ?>
                            </form>
                        <br /><br/>
                    </div>
                    <div class="col-md-8">
                        <div class="alert alert-info">
                            <!--<h2>User Bio : </h2>-->
                            <form class="pull-right"action="profileEdit.php" method="post">
                                <input type="hidden" name="id" value="<?php echo $id ?>">
                                <?php if ($id == $_SESSION['id']) { ?>
                                    <input class="btn btn-success" type="submit" value="Edit Profile">
                                <?php } ?>
                            </form>
                            <h4>User Information</h4>
                        </div>
                            <div class="col-md-12">
                                <table class="table table-striped table-hover">
                                    <tr> <td>ID </td> <td><?php echo $oneUser['user_id'] ?></td></tr>
                                    <tr><td>FIrst Name </td> <td><?php
                                            if (isset($oneUser['first_name']) && !empty($oneUser['first_name'])) {
                                                echo $oneUser['first_name'];
                                            } else {
                                                echo "No data available";
                                            }
                                            ?>
                                    <tr><td>Last Name </td> <td><?php
                                            if (isset($oneUser['last_name']) && !empty($oneUser['last_name'])) {
                                                echo $oneUser['last_name'];
                                            } else {
                                                echo "No data available";
                                            }
                                            ?>
                                    <tr><td>Personal Phone</td> <td><?php
                                            if (isset($oneUser['personal_phone']) && !empty($oneUser['personal_phone'])) {
                                                echo $oneUser['personal_phone'];
                                            } else {
                                                echo "No data available";
                                            }
                                            ?>
                                    <tr><td>Home Phone</td> <td><?php
                                            if (isset($oneUser['home_phone']) && !empty($oneUser['home_phone'])) {
                                                echo $oneUser['home_phone'];
                                            } else {
                                                echo "No data available";
                                            }
                                            ?>
                                    <tr><td>Office Phone</td> <td><?php
                                            if (isset($oneUser['office_phone']) && !empty($oneUser['office_phone'])) {
                                                echo $oneUser['office_phone'];
                                            } else {
                                                echo "No data available";
                                            }
                                            ?>
                                    <tr><td>Current Address</td> <td><?php
                                            if (isset($oneUser['current_address']) && !empty($oneUser['current_address'])) {
                                                echo $oneUser['current_address'];
                                            } else {
                                                echo "No data available";
                                            }
                                            ?>
                                    <tr><td>Permanent Address</td> <td><?php
                                            if (isset($oneUser['permanent_address']) && !empty($oneUser['permanent_address'])) {
                                                echo $oneUser['permanent_address'];
                                            } else {
                                                echo "No data available";
                                            }
                                            ?>


                                        </td>
                                    </tr>
                                    <tr><td>Created At </td> <td><?php echo $oneUser['created_at'] ?></td></tr>
                                    <tr><td>Last Modified</td> <td><?php echo $oneUser['modified_at'] ?></td></tr>
                                </table>
                            </div>
                           
                        </div>
                    </div>
                </div>
                <!-- ROW END -->


            </section>
            <!-- SECTION END -->
        </div>
        <!-- CONATINER END -->  

    </body>
</html>
