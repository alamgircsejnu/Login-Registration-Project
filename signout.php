<?php
include './navigation.php';
include_once './vendor/autoload.php';

use App\BITM\SEIP108594\users\Users;
if(isset($_SESSION['id'])){
    $id = $_SESSION['id'];
}
session_destroy();

session_start();
$user = new Users();
$oneUser = $user->showProfile($id);
$_SESSION['Message'] = '<h3>Goodbye, <b>'.$oneUser['first_name'].' '.$oneUser['last_name'].'</b></h3>';

header('Location:index.php');

