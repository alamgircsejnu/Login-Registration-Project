<?php session_start(); ?>
<html>
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <meta name="description" content="" />
    <meta name="author" content="" />
    <!--[if IE]>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <![endif]-->
    <title>Mini Project|Login-Registration|User Profile</title>
    <!-- BOOTSTRAP STYLE SHEET -->
    <link href="assets/css/bootstrap.css" rel="stylesheet" />
    <!-- FONT-AWESOME STYLE SHEET FOR BEAUTIFUL ICONS -->
    <link href="assets/css/font-awesome.css" rel="stylesheet" />
     <!-- CUSTOM STYLE CSS -->
    <style type="text/css">
               .btn-social {
            color: white;
            opacity: 0.8;
        }

            .btn-social:hover {
                color: white;
                opacity: 1;
                text-decoration: none;
            }

        .btn-facebook {
            background-color: #3b5998;
        }

        .btn-twitter {
            background-color: #00aced;
        }

        .btn-linkedin {
            background-color: #0e76a8;
        }

        .btn-google {
            background-color: #c32f10;
        }
    </style>
</head>
<body>
    <div class="navbar navbar-inverse navbar-static-top">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#">Design Bootstrap</a>
            </div>
            <div class="navbar-collapse collapse">
                <ul class="nav navbar-nav navbar-right ">
                        <li role="presentation"><a href="index.php">Home</a></li>
                        <?php if (isset($_SESSION['user_group_id']) && $_SESSION['user_group_id'] == '1') { ?>
                            <li role="presentation"><a href="activated.php">Active User List</a></li>
                            <li role="presentation"><a href="userList.php">Inactive User List</a></li>
                        <?php } else { ?>
                        <?php } ?>
                        <?php if (isset($_SESSION['id']) && !empty($_SESSION['id'])) { ?>
                            <li role="presentation"><a href="profile.php">User Profile</a></li>
                        <?php } else { ?>
                        <?php } ?>
                        <?php if (isset($_SESSION['id']) && !empty($_SESSION['id'])) { ?>
                        <?php } else { ?>
                            <li role="presentation" class="navbar-right"><a href="signup.php">Sign UP</a></li>
                        <?php } ?>
                        <?php if (isset($_SESSION['id']) && !empty($_SESSION['id'])) { ?>
                            <li role="presentation" class="navbar-right"><a href="signout.php">Sign Out</a></li>
                        <?php } else { ?>
                            <li role="presentation" class="navbar-right"><a href="signin.php">Sign In</a></li>
                        <?php } ?>
                    </ul>
            </div>

        </div>
    </div>
    <!-- NAVBAR CODE END -->

    <script src="assets/js/jquery-1.11.1.js"></script>
    <!-- REQUIRED BOOTSTRAP SCRIPTS -->
    <script src="assets/js/bootstrap.js"></script>
</body>

</html>
