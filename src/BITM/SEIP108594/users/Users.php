<?php

namespace App\BITM\SEIP108594\users;

//session_start();
class Users {

    public $id = '';
    public $username = '';
    public $password = '';
    public $firstName = '';
    public $lastName = '';
    public $personalPhone = '';
    public $homePhone = '';
    public $officePhone = '';
    public $currentAddress = '';
    public $permanentAddress = '';
    public $profilePicture = '';

    public function __construct() {
//        session_start();
        $conn = mysql_connect('localhost', 'root', '')or die("Server Not Found");
        mysql_select_db('usrreg') or die("Database Not Found");
    }

    public function prepare($data = '') {

//        echo "<pre>";
//        print_r($data);
//        die();
        if (array_key_exists('id', $data)) {
            $this->id = $data['id'];
        }
        if (array_key_exists('picture', $data)) {
            $this->profilePicture = $data['picture'];
        }
        if (array_key_exists('username', $data)) {
            $this->username = $data['username'];
        }
        if (array_key_exists('email', $data)) {
            $this->email = $data['email'];
        }
        if (array_key_exists('password', $data)) {
            $this->password = $data['password'];
        }
        if (array_key_exists('firstName', $data)) {
            $this->firstName = $data['firstName'];
        }
        if (array_key_exists('lastName', $data)) {
            $this->lastName = $data['lastName'];
        }
        if (array_key_exists('personalPhone', $data)) {
            $this->personalPhone = $data['personalPhone'];
        }
        if (array_key_exists('homePhone', $data)) {
            $this->homePhone = $data['homePhone'];
        }
        if (array_key_exists('officePhone', $data)) {
            $this->officePhone = $data['officePhone'];
        }
        if (array_key_exists('currentAddress', $data)) {
            $this->currentAddress = $data['currentAddress'];
        }
        if (array_key_exists('permanentAddress', $data)) {
            $this->permanentAddress = $data['permanentAddress'];
        }
//        print_r($this);
//        die();
    }

    public function signUp() {
        if (isset($this->username) && !empty($this->username) && isset($this->password) && !empty($this->password)) {
            $query = "INSERT INTO `usrreg`.`users` (`unique_id`, `username`, `password`, `email`,  `created_at`) VALUES ('" . uniqid() . "','" . $this->username . "', '" . $this->password . "', '" . $this->email . "', '" . date('Y-m-d') . "')";
            $query2 = "INSERT INTO `usrreg`.`profiles` (`user_id`) VALUES (LAST_INSERT_ID());";
//            echo $query;
//            die();
            mysql_query($query);
            if (mysql_query($query2)) {
                $_SESSION['Message'] = '<h2>Successfully Registered. Wait for admin approval.</h2>';
            } else {
                $_SESSION['Message'] = '<h2>Registration Failed! Data Already Exists</h2>';
            }
        }
        header('location:../../../../index.php');
    }

    public function showProfile($id = '') {
        $this->id = $id;
        $query = "SELECT * FROM `profiles` where user_id=" . $this->id;
//        echo $query;
//        die();
        $result = mysql_query($query);
        $row = mysql_fetch_assoc($result);
        return $row;
    }

    public function index() {
        $mydata = array();
        $query = "SELECT * FROM `users` WHERE is_active IS NULL";
//        echo $query;
//        die();
        $result = mysql_query($query);
        while ($row = mysql_fetch_assoc($result)) {
            $mydata[] = $row;
        }
        return $mydata;
        header('location:index.php');
    }

    public function signin() {
        $query = "SELECT * FROM `users` WHERE `username`='$this->username' AND `is_active`=1";
//        echo $query;
//        die();
        $result = mysql_query($query);
        $row = mysql_fetch_assoc($result);
        return $row;
    }

    public function updateActive() {
        $query = "UPDATE `usrreg`.`users` SET  `is_active` = 1 WHERE `users`.`id` =" . $this->id;

//        echo $query;
//        die();
        mysql_query($query);
        $_SESSION['Message'] = "<h2>" . "User Account Activated" . "</h2>";
        header('location:../../../../index.php');
    }

    public function active() {
        $mydata = array();
        $query = "SELECT * FROM `users` WHERE `is_active` = 1";
//        echo $query;
//        die();
        $result = mysql_query($query);
        while ($row = mysql_fetch_assoc($result)) {
            $mydata[] = $row;
        }
        return $mydata;
        header('location:../../../../index.php');
    }

    public function updateProfile() {
        $query = "UPDATE `usrreg`.`profiles` SET `first_name` = '" . $this->firstName . "', `last_name` = '" . $this->lastName . "', `personal_phone` = '" . $this->personalPhone . "', `home_phone` = '" . $this->homePhone . "', `office_phone` = '" . $this->officePhone . "', `current_address` = '" . $this->currentAddress . "', `permanent_address` = '" . $this->permanentAddress . "', `modified_at` = '" . date('Y-m-d') . "' WHERE `profiles`.`user_id` = " . $this->id;
//        echo $query;
//        die();
        if (mysql_query($query)) {
            $_SESSION['Message'] = '<h4>Profile Updated Successfully</h4>';
        } else {
            $_SESSION['Message'] = '<h4>Oops!!! Something wrong to update profile.</h4>';
        }
        header('Location:../../../../profile.php');
    }

    public function updateProfilePicture() {
        $query = "UPDATE `usrreg`.`profiles` SET `profile_pic` = '" . $this->profilePicture . "' WHERE `profiles`.`user_id` = " . $this->id;
        if (mysql_query($query)) {
            $_SESSION['Message'] = '<h4>Profile Updated Successfully</h4>';
        } else {
            $_SESSION['Message'] = '<h4>Oops!!! Something wrong to update profile.</h4>';
        }
        header('Location:../../../../profile.php');
    }

}
